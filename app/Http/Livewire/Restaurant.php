<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Restaurant extends Component
{
    public $restaurantId;
    public function render()
    {
        return view('livewire.restaurant');
    }
}
