<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Wellness extends Component
{
    public function render()
    {
        return view('livewire.wellness');
    }
}
