<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/restaurant/--slideshow-1--slide-5.jpg" class="img-fluid">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-info h1">
                <b>Lorem Ipsum</b>
            </p>
            <p class="text-dark mt-3 h5"> 
                Maecenas tempor, ipsum eget varius finibus, est urna sollicitudin enim, maximus sodales lectus eros ut nibh
            </p>
            <p class="text-dark h5"> 
                    Sed sed porttitor nunc, non tincidunt mi. Interdum et malesuada fames ac ante ipsum primis in faucibus
            </p>
            <p class="text-dark h5"> 
                    risus, quis sollicitudin sem pretium a. Curabitur tristique erat vel risus ultricies, sed mollis orci egestas
            </p>
            <p class="text-dark h5"> 
                    purus. Sed sed porttitor nunc, non tincidunt mi. Interdum et malesuada fames ac ante ipsum primis in faucibus
            </p> 
            <p class="text-dark h5"> 
                    molestie diam dui, vitae elementum velit ornare sit amet.
            </p>
            <p class="text-dark h1 mt-5">
                <b>Öffnungszeiten</b>
            </p>
            <p class="text-dark h5">
                Restaurant Deichperle: 
            </p>
            <p class="text-dark h5">
                07.00 – 10.30 
            </p>
            <p class="text-dark h5">
                Reservierung
            </p>
            <p class="text-dark h5">
                R UNSER  HOTELGÄSTE
            </p>
            <p class="text-dark h5">
                13.00 – 16.30 Uhr
            </p>
            <p class="text-dark h5">
                14.00 – 16.30 Uhr Kaffee
            </p>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="container mt-5">
            <div class="d-none d-lg-block">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4 col-md-12 text-center">
                        <img src="/images/restaurant/logo-deichperle.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5 col-md-12 text-center" style="margin-top:-122px;">
                        <img src="/images/restaurant/restaurant--freeform-1--slot-3.jpg" class="img-fluid">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 text-center mt-5">
                        <p class="text-light h5 mt-5">
                            "Sed ut perspiciatis unde omnis iste natus error sit voluptatem  
                        </p>
                        <p class="text-light h5">
                            pleasure of the moment, so blinded by desire
                        </p>
                        <p class="text-light h5">
                            belongs to those who fail in their duty through weakness of will
                        </p>
                        <p class="text-light h5">
                            The wise man therefore always holds in these matters to this principle of 
                        </p>
                        <p class="text-light h5">
                            will frequently occur that pleasures have to be repudiated and annoyances
                        </p>
                        <p class="text-light h5">
                             matters to this principle of selection: he rejects pleasures to secure other
                        </p>
                        <p class="text-light h5">
                            libero quis tincidunt. Donec faucibus nibh quam. Nulla ullamcorper sagittis
                        </p>
                        <p class="text-light h5">
                            porta libero condimentum. Cras purus est, blandit eget consectetur non
                        </p>
                        <p class="text-light h5">
                            justo sodales, vitae fringilla est condimentum. Duis venenatis eu enim vel 
                        </p>
                        <p class="text-light h5">
                            justo ac lectus egestas, nec pulvinar arcu consectetur. Donec ultrices nisi
                        </p>
                        <p class="text-light h5">
                             eu convallis est eleifend id
                        </p>
                    </div>
                    <div class="col-lg-5 text-center">
                        <img src="/images/restaurant/hotel--freeform-2--slot-1.jpg" class="img-fluid">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="/images/restaurant/erle--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="/images/restaurant/eshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="/images/restaurant/perle--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true">
                                </span>
                                <span class="visually-hidden">
                                    Previous
                                </span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true">
                                </span>
                                <span class="visually-hidden">
                                    Next
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-block d-lg-none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/restaurant/logo-deichperle.svg" class="img-fluid">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center mt-5" style="margin-top:-122px;">
                        <img src="/images/restaurant/restaurant--freeform-1--slot-3.jpg" class="img-fluid">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 text-center mt-5">
                        <p class="text-light h5 mt-5">
                            "Sed ut perspiciatis unde omnis iste natus error sit voluptatem  
                        </p>
                        <p class="text-light h5">
                            pleasure of the moment, so blinded by desire
                        </p>
                        <p class="text-light h5">
                            belongs to those who fail in their duty through weakness of will
                        </p>
                        <p class="text-light h5">
                            The wise man therefore always holds in these matters to this principle of 
                        </p>
                        <p class="text-light h5">
                            will frequently occur that pleasures have to be repudiated and annoyances
                        </p>
                        <p class="text-light h5">
                             matters to this principle of selection: he rejects pleasures to secure other
                        </p>
                        <p class="text-light h5">
                            libero quis tincidunt. Donec faucibus nibh quam. Nulla ullamcorper sagittis
                        </p>
                        <p class="text-light h5">
                            porta libero condimentum. Cras purus est, blandit eget consectetur non
                        </p>
                        <p class="text-light h5">
                            justo sodales, vitae fringilla est condimentum. Duis venenatis eu enim vel 
                        </p>
                        <p class="text-light h5">
                            justo ac lectus egestas, nec pulvinar arcu consectetur. Donec ultrices nisi
                        </p>
                        <p class="text-light h5">
                             eu convallis est eleifend id
                        </p>
                    </div>
                    <div class="col-lg-5 text-center mt-5">
                        <img src="/images/restaurant/hotel--freeform-2--slot-1.jpg" class="img-fluid">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 mt-5">
                        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="/images/restaurant/erle--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="/images/restaurant/eshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="/images/restaurant/perle--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true">
                                </span>
                                <span class="visually-hidden">
                                    Previous
                                </span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true">
                                </span>
                                <span class="visually-hidden">
                                    Next
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-info py-5">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-2 col-md-1 mt-5 text-center">
                <img src="/images/restaurant/-bastianinselmann_.jpg" class="img-fluid" style="border-radius:122px;">
            </div>
            <div class="col-lg-5 text-center">
                <p class="text-danger h1 mt-5">
                    Generated 5 paragraphs
                </p>
                <p class="text-light h5 mt-3">
                     vel commodo. Praesent vitae felis mauris
                </p>
                <p class="text-light h5">
                    Donec ultrices nisi magna, eu convallis est 
                </p>
                <p class="text-light h5 mt-4">
                    <span class="text-danger">
                        <b>T</b>
                    </span>
                    00128392
                </p>
                <p class="text-light h5">
                    <span class="text-danger">
                        <b>M</b>
                    </span>
                    masjdios@hotmail.com
                </p>
            </div>
        </div>
    </div>
</div>
