<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/restaurant/how1--slide4.jpg" class="img-fluid">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <p class="text-info h1 mt-5">
                <b>What is Lorem Ipsum</b>
            </p>
            <p class="text-dark h5 mt-3">
                Lorem Ipsum is simply dummy text of the printing and typesetting 
            </p>
            <p class="text-dark h5">
                industry. Lorem Ipsum has been the industry's standard dummy 
            </p>
            <p class="text-dark h5">
                text ever since the 1500s, when an unknown printer took a galley 
            </p>
            <p class="text-dark h5">
                of type and scrambled it to make a type specimen book. It has 
            </p>
            <p class="text-dark h5">
                survived not only five centuries, but also the leap into electronic 
            </p>
            <p class="text-dark h3 mt-5">
                <b>Öffnungszeiten</b>
            </p>
            <p class="text-dark h5 mt-2">
                09.30 – 19.00 Spa
            </p>
            <p class="text-dark h5">
                07.30 – 21.00 Pool
            </p>
            <p class="text-dark h5">
                11.00 – 21.00 Sauna
            </p>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="row mt-5">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 col-md-8 text-center" style="margin-top:-122px;">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/images/spa/eshow1--slide1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spa/--slide2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spa/w1--slide3.jpg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <p class="text-info h1">
                    <b>Where does it come from?</b>
                </p>
                <p class="text-light h5 mt-3">
                    Contrary to popular belief, Lorem Ipsum is not simply random text. 
                </p>
                <p class="text-light h5">
                    It has roots in a piece of classical Latin literature from 45 BC, 
                </p>
                <p class="text-light h5">
                    making it over 2000 years old. Richard McClintock, a Latin 
                </p>
                <p class="text-light h5">
                    the more obscure Latin words, consectetur
                </p>
                <p class="text-light h5">
                    et Malorum" (The Extremes of Good and Evil) by Cicero
                </p>
                <p class="text-light h5 mt-5">
                   45 BC. This book is a treatise on the theory of ethics, very popular  
                </p>
                <p class="text-light h5">
                    during the Renaissance. The first line of Lorem Ipsum, "Lorem 
                </p>
                <p class="text-light h3 mt-5">
                    <b>Day Spa Preise für externe Gäste</b>
                </p>
                <p class="text-light h5">
                    <b>1 Tage- 40 -¢</b>
                    
                </p>
                <p class="text-light h5">
                    <b>6 Stunden- 30 -¢</b>
                </p>
                <p class="text-light h5">
                    <b>3 Stunden- 15 -¢</b>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-1.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-2.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-3.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-4.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-5.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
                <div class="col-lg-4 col-md-12 text-center mt-5">
                    <img src="/images/spa/wellness--grid-6.jpg" class="img-fluid">
                    <p class="text-info mt-4 text-start h1">
                        <b>Where</b>
                    </p>
                    <p class="text-light mt-3 h5 text-start">
                        <b>Individuelleee Massage</b>
                    </p>
                    <p class="text-light text-start h5">
                        38 min – 42,– €
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-info py-5">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-2 col-md-1 mt-5 text-center">
                <img src="/images/spa/wellness-spa--team.jpg" class="img-fluid" style="border-radius:122px;">
            </div>
            <div class="col-lg-5 text-center">
                <p class="text-danger h1 mt-5">
                    Generated 5 paragraphs
                </p>
                <p class="text-light h5 mt-3">
                     vel commodo. Praesent vitae felis mauris
                </p>
                <p class="text-light h5">
                    Donec ultrices nisi magna, eu convallis est 
                </p>
                <p class="text-light h5 mt-4">
                    <span class="text-danger">
                        <b>T</b>
                    </span>
                    00128392
                </p>
                <p class="text-light h5">
                    <span class="text-danger">
                        <b>M</b>
                    </span>
                    masjdios@hotmail.com
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-12 text-center">
        <img src="/images/spa/wellness--full-width.jpg" class="img-fluid">
    </div>
</div>
