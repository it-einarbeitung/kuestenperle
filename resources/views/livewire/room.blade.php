<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/room/mmer--slideshow-1--slide-5.jpg" class="img-fluid">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 text-center">
                <p class="text-info h1 mt-5">
                    What is Lorem Ipsum
                </p>
            <p class="text-dark h5 mt-5">
                Ankommen, einchecken und sich rundum wohlfühlen. Mal für ein Wochenende mit dem Herzblatt, mal für eine Woche mit Kind & Kegel. Wie heißt es so schön? „Wie man sich bettet, so liegt man.“ Und wir möchten, dass Sie gut schlafen. Wir haben für jeden eine gute Stube. Alle nordisch-frisch und fröhlich eingerichtet. Barrierefrei, inklusive WLAN gratis im ganzen Haus, kostenlose Nutzung des Wellness- und Fitnessbereichs sowie ausreichenden Parkplätzen (5,– € pro Übernachtung) vor der Tür. Und den tollen Meerblick gibt es in voller und ganzer Pracht bereits ab der zweiten Etage, trotz des Deiches! Sie benötigen ein behinderten- oder rollstuhlfreundliches Zimmer oder wollen Ihren Hund mitbringen? Kein Problem. Sie können dies einfach beim Buchungsvorgang auf unserer Website mit angeben oder Sie kontaktieren uns hierzu direkt.
            </p>
            </div>
        </div>
    </div>
    <div class="row mt-5 text-center">
        <div class="col-lg-2">
            <p>
                <a class="btn text-link text-info" data-bs-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample1">
                    <b>Standarzimmer</b>
                </a>
            </p>
        </div>
        <div class="col-lg-2">
            <p>
                <a class="btn text-link text-info" data-bs-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
                    <b>Link with href</b>
                </a>
            </p>
        </div>
        <div class="col-lg-2">
            <p>
                <a class="btn text-link text-info" data-bs-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample3">
                    <b>Familienzimmer</b>
                </a>
            </p>
        </div>
    </div>
    <div class="bg-danger py-5">
        <div class="collapse" id="collapseExample1">
            <div class="row">
                <div class="col-lg-6 text-center ">
                    <p class="text-info h3 mt-5">
                        Where does it come from?
                    </p>
                    <p class="text-light h5 mt-5">
                        Contrary to popular belief, Lorem Ipsum is not simply random text
                    </p> 
                    <p class="text-light h5">
                        It has roots in a piece of classical Latin literature from 45 BC
                    </p> 
                    <p class="text-light h5">
                        ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    </p> 
                    <p class="text-light h5">
                        versions from the 1914 translation by H. Rackham.
                    </p> 
                   <div id="carouselExampleIndicators" class="carousel slide mt-5" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/room/suite-slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/immer--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/dardzimmer--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Previous
                            </span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Next
                            </span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="/images/room/-slot-2.jpg" class="img-fluid">
                    <p class="text-light mt-5 h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                    </p>
                    <p class="text-light h5">
                        Sed vel enim finibus, scelerisque ante elementum
                    </p>
                    <p class="text-light h5">
                         In venenatis tristique facilisis. Mauris
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Maecenas tempor, ipsum eget varius finibus
                    </p>
                    <p class="text-light h5">
                        est urna sollicitudin enim, maximus sodales lectus eros.
                    </p>
                    <p class="text-light h5">
                         Interdum et malesuada fames ac ante ipsum primis.
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief
                    </p>
                    <p class="text-light h3 mt-5">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <p class="text-light h3">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <button type="button" class=" btn btn-outline-info bg-info text-light mt-5" style="border-radius:0px;">
                        JETZT bUCHEN
                    </button>
                </div>
            </div>
        </div>

        <div class="collapse" id="collapseExample2">
            <div class="row">
                <div class="col-lg-6 text-center ">
                    <p class="text-info h3 mt-5">
                        Where does it come from?
                    </p>
                    <p class="text-light h5 mt-5">
                        Contrary to popular belief, Lorem Ipsum is not simply random text
                    </p> 
                    <p class="text-light h5">
                        It has roots in a piece of classical Latin literature from 45 BC
                    </p> 
                    <p class="text-light h5">
                        ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    </p> 
                    <p class="text-light h5">
                        versions from the 1914 translation by H. Rackham.
                    </p> 
                   <div id="carouselExampleIndicators" class="carousel slide mt-5" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/room/mmer--slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/immer--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/dardzimmer--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Previous
                            </span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Next
                            </span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="/images/room/mer-slot-2.jpg" class="img-fluid">
                    <p class="text-light mt-5 h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                    </p>
                    <p class="text-light h5">
                        Sed vel enim finibus, scelerisque ante elementum
                    </p>
                    <p class="text-light h5">
                         In venenatis tristique facilisis. Mauris
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Maecenas tempor, ipsum eget varius finibus
                    </p>
                    <p class="text-light h5">
                        est urna sollicitudin enim, maximus sodales lectus eros.
                    </p>
                    <p class="text-light h5">
                         Interdum et malesuada fames ac ante ipsum primis.
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief
                    </p>
                    <p class="text-light h3 mt-5">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <p class="text-light h3">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <button type="button" class=" btn btn-outline-info bg-info text-light mt-5" style="border-radius:0px;">
                        JETZT bUCHEN
                    </button>
                </div>
            </div>
        </div>

        <div class="collapse" id="collapseExample3">
            <div class="row">
                <div class="col-lg-6 text-center ">
                    <p class="text-info h3 mt-5">
                        Where does it come from?
                    </p>
                    <p class="text-light h5 mt-5">
                        Contrary to popular belief, Lorem Ipsum is not simply random text
                    </p> 
                    <p class="text-light h5">
                        It has roots in a piece of classical Latin literature from 45 BC
                    </p> 
                    <p class="text-light h5">
                        ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    </p> 
                    <p class="text-light h5">
                        versions from the 1914 translation by H. Rackham.
                    </p> 
                   <div id="carouselExampleIndicators" class="carousel slide mt-5" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/room/mmer--slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/immer--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/room/dardzimmer--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Previous
                            </span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Next
                            </span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="/images/room/-standardzimmer-slot-2.jpg" class="img-fluid">
                    <p class="text-light mt-5 h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                    </p>
                    <p class="text-light h5">
                        Sed vel enim finibus, scelerisque ante elementum
                    </p>
                    <p class="text-light h5">
                         In venenatis tristique facilisis. Mauris
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief, Lorem Ipsum is not
                    </p>
                    <p class="text-light h5">
                        Maecenas tempor, ipsum eget varius finibus
                    </p>
                    <p class="text-light h5">
                        est urna sollicitudin enim, maximus sodales lectus eros.
                    </p>
                    <p class="text-light h5">
                         Interdum et malesuada fames ac ante ipsum primis.
                    </p>
                    <p class="text-light h5">
                        Contrary to popular belief
                    </p>
                    <p class="text-light h3 mt-5">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <p class="text-light h3">
                        <b>Einzelzimmer ab 00,– €</b>
                    </p>
                    <button type="button" class=" btn btn-outline-info bg-info text-light mt-5" style="border-radius:0px;">
                        JETZT bUCHEN
                    </button>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <img src="/images/room/zimmer-suiten--full-width.jpg" class="img-fluid">
            </div>
        </div>
    </div>
</div>

