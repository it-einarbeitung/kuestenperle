<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/days/tagen-feiern--salon_1-slot-1.jpg" class="img-fluid">
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center">
            <p class="text-info h1">
                <b>Where does it come from?</b>
            </p>
            <p class="text-dark h5 mt-3">
                Contrary to popular belief, Lorem Ipsum is not simply random text. 
            </p>
            <p class="text-dark h5">
                It has roots in a piece of classical Latin literature from 45 BC, 
            </p> 
            <p class="text-dark h5">
                making it over 2000 years old. Richard McClintock, a Latin 
            </p>
            <p class="text-dark h5">
                professor at Hampden-Sydney College in Virginia, looked up one of 
            </p>
            <p class="text-dark h5">
                the more obscure Latin words, consectetur, from a Lorem Ipsum 
            </p>
            <p class="text-dark h5">
                passage, and going through the cites of the word in classical  
            </p>
            <p class="text-dark h5">
                ipsum dolor sit amet  
            </p>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-6 col-md-12 text-center mt-5">
                <p class="text-info h1 text-center mt-5">
                    <b>Rackham</b>
                </p>
                <p class="text-light h5 mt-3">
                    It is a long established fact that a reader will be distracted by the 
                </p>
                <p class="text-light h5">
                    readable content of a page when looking at its layout. The point of 
                </p>
                <p class="text-light h5">
                    injected humour and the like).
                </p>
                <img src="/images/days/tagen-feiern--salon_1-slot-1.jpg" class="img-fluid mt-4">
            </div>
            <div class="col-lg-3 col-md-12 text-center mt-5">
                <img src="/images/days/tagen-feiern--salon_1-slot-2.jpg" class="img-fluid mt-4">
                <p class="text-light h5 mt-5">
                    cases are perfectly simple and easy to distinguish.
                </p>
                <p class="text-light h5">
                    when our power of choice is untrammelled and when nothing prevents 
                </p>
                <p class="text-light h5">
                    our being able to do what we like best, 
                </p>
                <p class="text-light h5">
                    every pleasure is to be welcomed, 
                </p>
                <button class="btn btn-outline-info bg-info text-light mt-3">
                    <b>ANFARGEN</b>
                </button>
            </div>
        </div>
    </div>
    <div class="bg-info py-5">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-2 col-md-1 mt-5 text-center">
                <img src="/images/days/hotel-team-member-7-christinreisewitz_0.jpg" class="img-fluid" style="border-radius:122px;">
            </div>
            <div class="col-lg-5 text-center">
                <p class="text-danger h1 mt-5">
                    Generated 5 paragraphs
                </p>
                <p class="text-light h5 mt-3">
                     vel commodo. Praesent vitae felis mauris
                </p>
                <p class="text-light h5">
                    Donec ultrices nisi magna, eu convallis est 
                </p>
                <p class="text-light h5 mt-4">
                    <span class="text-danger">
                        <b>T</b>
                    </span>
                    00128392
                </p>
                <p class="text-light h5">
                    <span class="text-danger">
                        <b>M</b>
                    </span>
                    masjdios@hotmail.com
                </p>
            </div>
        </div>
    </div>
</div>
