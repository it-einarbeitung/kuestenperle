<div>
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-info h1">
                    <b>Fofftein för de ganze Familie.</b>
                </p>
                <p class="text-dark h5 mt-5">
                    Willkommen an der Nordsee, willkommen in der Büsumer Perlebucht! Der Name ist hier Programm: Wir 
                </p>
                <p class="text-dark h5">
                    finden, dieses Fleckchen Erde ist etwas ganz Besonderes – eine Perle eben. Für „Nur-einmal-kurz-die-
                </p>
                <p class="text-dark h5">
                    Nase-in-den-Wind-halten“ oder für ganze Urlaubstage. In Gummistiefeln oder Badelatschen. Voller 
                </p>
                <p class="text-dark h5">
                    Action & Sport sowie mit Muße und Entspannung.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center mt-5">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/images/frontpage--slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/frontpage--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/frontpage--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/frontpage--slideshow-1--slide-5.jpg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-3"></div>
                <div class="col-lg-3 text-end">
                    <img src="/images/restaurant--freeform-2--slot-4.jpg" class="img-fluid" style="margin-top: -135px;">
                </div>
                <div class="col-lg-6 text-center mt-5">
                    <<i class="fab fa-gitkraken text-info h1 mt-5"></i>
                    <p class="text-info h1">
                       <b> Ahoi …</b>
                    </p>
                    <p class="text-light h5 mt-4">
                        … heißt es in der Küstenperle, dem neuen Vier-Sterne-Strandhotel & Spa in  
                    </p>
                    <p class="text-light h5">
                        Büsum. Hier fühlt sich jeder wohl – allein, zu zweit und insbesondere auch 
                    </p>
                    <p class="text-light h5">
                        Familien. Das Restaurant Deichperle, die Bar Schneiders und der Spa- & 
                    </p>
                    <p class="text-light h5">
                        Wellnessbereich zeichnen uns aus. Und natürlich die liebevoll eingerichteten 
                    </p>
                    <p class="text-light h5">
                        Zimmer und Suiten. Alles mit frisch-nordisch-gemütlichem Flair.
                    </p>
                    <button type="button" class="btn btn-outline-info bg-info text-light h4 mt-3">
                        <b>3D RUNDGANG STARTEN</b>
                    </button>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-4">
                    <img src="/images/hotel--freeform-2--slot-1.jpg" class="img-fluid" style="margin-bottom:-90px;">
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center mt-5">
                    <img src="/images/restaurant-bar-deichperle--slideshow-1--slide-1.jpg" class="img-fluid mt-5">
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-3"></div>
                <div class="col-lg-3 text-center">
                    <img src="/images/restaurant--freeform-2--slot-4.jpg" class="img-fluid" style="margin-top: -135px; width:300px;">
                </div>
                <div class="col-lg-6 text-center mt-5">
                    <img src="/images/hotel--freeform-2--slot-1.jpg" class="img-fluid"style="width:300px;" >
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6 text-center">
                    <i class="fab fa-gitkraken text-info h1 mt-5"></i>
                    <p class="text-info h1">
                       <b> Ahoi …</b>
                    </p>
                    <p class="text-light h5 mt-4">
                        … heißt es in der Küstenperle, dem neuen Vier-Sterne-Strandhotel & Spa in  
                    </p>
                    <p class="text-light h5">
                        Büsum. Hier fühlt sich jeder wohl – allein, zu zweit und insbesondere auch 
                    </p>
                    <p class="text-light h5">
                        Familien. Das Restaurant Deichperle, die Bar Schneiders und der Spa- & 
                    </p>
                    <p class="text-light h5">
                        Wellnessbereich zeichnen uns aus. Und natürlich die liebevoll eingerichteten 
                    </p>
                    <p class="text-light h5">
                        Zimmer und Suiten. Alles mit frisch-nordisch-gemütlichem Flair.
                    </p>
                    <button type="button" class="btn btn-outline-info bg-info text-light h4 mt-3">
                        <b>3D RUNDGANG STARTEN</b>
                    </button>
                </div>
                <div class="col-lg-4 text-center mt-5">
                    <img src="/images/restaurant-bar-deichperle--slideshow-1--slide-1.jpg" class="img-fluid mt-5" style="margin-bottom:-80px; width: 300px;">
                </div>
            </div>
        </div>
    </div>
    <div class="bg-info py-5">
        <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6 text-center mt-5">
                <p class="text-danger h1">
                    <b>Eine Handvoll Glück.</b>
                </p>
                <p class="text-light h5 mt-5">
                    Sie sind von der Küstenperle begeistert und wollen 
                </p>
                <p class="text-light h5">
                    auch anderen diese Glücksmomente bereiten? Dann 
                </p>
                <p class="text-light h5">
                    verschenken Sie doch einfach einen unserer 
                </p>
                <p class="text-light h5">
                    Gutscheine. Mehr dazu hier:
                </p>
                <button type="button" class="btn btn-outline-danger bg-danger text-light mt-3" style="width:200px;">
                    <b>GUTSCHEN</b>
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-1"></div>
        <div class="col-lg-4 text-center mt-5">
            <img src="/images/ShopKP_40von99_53dd1e82-05cf-4a8d-9fcb-1f812144823d_2048x.jpg" class="img-fluid">
            <p class="text-info text-start">
                Onlineshop
            </p>
            <p class="text-info text-start h1">
               <b>Küstenfeeling für<br> Zuhause.</b>
            </p>
            <p class="text-dark h5 mt-5">
                Ob als Erinnerung an die schönen Urlaubstage oder zur 
            </p>
            <p class="text-dark h5">
                Überbrückung bis zum Wiedersehen. Für Sie selber oder 
            </p>
            <p class="text-dark h5">
                Ihre Liebsten. In unserem Onlineshop finden Sie Ihr 
            </p>
            <p class="text-dark h5">
                Küstenglück to-go - vom Sweater bis zur Tasse. 
            </p>
            <button type="button" class="btn btn-outline-info bg-info text-light mt-4">
                <b>WEBSHOP BESUCHEN</b>
            </button>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-4 text-center">
            <img src="/images/hotel--freeform-1--slot-3.jpg" class="img-fluid">
            <p class="text-info text-start">
                Buchungsspecial
            </p>
            <p class="text-info text-start h1">
               Immer wieder <br>sonntags …
            </p>
            <p class="text-dark h5 mt-5">
                … kommt die Entspannung! Buchen Sie jetzt Ihre 
            </p>
            <p class="text-dark h5">
               Sonntagnacht in der Küstenperle inklusive Halbpension  
            </p>
            <p class="text-dark h5">
                zum unschlagbaren Rabatt auf die Tagesrate!
            </p>
            <p class="text-dark h5 text-start mt-4">
                <b>ab 121,30 €</b>
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-info text-center h1">
                <b>Överall veel Spoß för de Lüüd.</b>
            </p>
            <p class="text-dark h5 mt-4">
                Hier gehen Dithmarscher Tradition mit nahezu unendlichem Freizeitvergnügen Hand in Hand. Besonders 
            </p>
            <p class="text-dark h5">
                wohltuend ist die Nähe zum Meer an der Familienlagune vor der Haustür. Oder haben Sie Lust auf eine 
            </p>
            <p class="text-dark h5">
                Tour durch Büsum? Von der Küstenperle aus ist es ein schöner Spaziergang, immer am Deich entlang. 
            </p>
            <p class="text-dark h5">
                Absolute Highlights für Junge & Ältere: die Events rund um Spaß, Sport & Musik.
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="ratio ratio-16x9">
                <iframe src="https://www.youtube.com/embed/HC938Ivdk1U" title="YouTube video" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <p class="text-light h1">
                    <b>Kiek mol, wat scheun</b>
                </p>
                <p class="text-info h4 mt-3">
                    @hotel-kuestenperle
                </p>
            </div>
        </div>
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-2">
                </div>  
                <div class="col-lg-2">
                    <img src="/images/1603_n.jpg" class="img-fluid border border-5"> 
                </div>
                <div class="col-lg-1">
                    <img src="/images/079323319_n.jpg"class="img-fluid border border-5">
                </div>
                <div class="col-lg-2">
                    <img src="/images/42674216338_n.jpg" class="img-fluid border border-5">
                </div>
                <div class="col-lg-1">
                    <img src="/images/24864_n.jpg"class="img-fluid border border-5">
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-1">
                    </div>
                    <div class="col-5">
                        <img src="/images/1603_n.jpg" class="img-fluid border border-5">
                    </div>
                    <div class="col-5">
                        <img src="/images/079323319_n.jpg" class="img-fluid border border-5">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-1">
                    </div>
                    <div class="col-5">
                        <img src="/images/42674216338_n.jpg" class="img-fluid border border-5">
                    </div>
                    <div class="col-5">
                        <img src="/images/24864_n.jpg" class="img-fluid border border-5">
                    </div>
                </div>
            </div>
        </div>
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-3">
                </div>  
                <div class="col-lg-1">
                    <img src="/images/322197796_n.jpg" class="img-fluid border border-5"> 
                </div>
                <div class="col-lg-2">
                    <img src="/images/419077874_n.jpg"class="img-fluid border border-5">
                </div>
                <div class="col-lg-3">
                    <img src="/images/269776_n.jpg" class="img-fluid border border-5">
                </div>
                <div class="col-lg-1">
                    <img src="/images/0818834_n.jpg"class="img-fluid border border-5">
                </div>
            </div>
        </div>
    </div>
    <div class="bg-info py-5">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-4 text-center mt-5">
                <p class="text-danger h1 mt-5">
                   <b> Küstenperle</b>
                </p>
            </div> 
        </div>
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-10 text-start">
                <p class="text-danger h4 mt-3">
                    <b>A</b>
                    <a href="#" class=" btn btn-link text-light text-start">
                        <b>Dithmarscher Str. 39<br>
                            25761 Büsum</b>
                    </a>
                </p>
                <p class="text-danger h4 mt-3">
                    <b>T</b>
                    <a href="#" class=" btn btn-link text-light text-start">
                        <b> + 49 . 48 34 . 96 211 - 0</b>
                    </a>
                    <br>
                    <b>F</b>
                    <a href="#" class=" btn btn-link text-light text-start">
                        <b>+ 49 . 48 34 . 96 211 - 600</b>
                
                    </a>
                </p>
                <p class="text-danger h4 mt-3">
                    <b>M</b>
                    <a href="#" class=" btn btn-link text-light text-start">
                        <b>info@hotel-kuestenperle.de</b>
                    </a>
                    <br>
                    <b>W</b>
                    <a href="#" class=" btn btn-link text-light text-start">
                        <b>www.hotel-kuestenperle.d</b>
                
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>
