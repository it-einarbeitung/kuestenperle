<div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger py-3">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation bg-light">
                <span class="navbar-toggler-icon text-danger">
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link me-5" aria-current="page" href="/team">
                            <b>HOTEL<br>
                            <span class="text-info">&</span> 
                                TEAM</b>
                        </a>
                        <button class="btn btn-outline-info bg-info text-light"style="border-radius: 0px; width: 120px;">
                                MELDE  
                        </button>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-light ms-5" href="/room">
                            <b>ZIMMER<br>
                            <span class="text-info">&</span> 
                                SUITEN
                            </b>
                        </a>
                    </li>
                    <li class="nav-item ms-5">
                        <a class="nav-link text-light" href="/restaurant">
                            <b>RESTAURANT<br>
                            <span class="text-info">&</sapn>
                                BAR
                            </b>
                        </a>
                        <button class="btn btn-outline-info bg-info text-light"style="border-radius: 0px; width: 120px;">
                                FAQ 
                        </button>
                    </li>
                    <li class="nav-item ms-5">
                        <a class="nav-link" href="/welcome">
                            <i class="fad fa-heart text-info h1"></i>
                        </a>
                    </li>
                    <li class="nav-item ms-5">
                        <a class="nav-link text-light" href="/wellness">
                            <b>WELLNESS<br>
                            <span class="text-info">&</sapn>
                                SPA
                            </b>
                        </a>
                        <button class="btn btn-outline-info bg-info text-light"style="border-radius: 0px; width: 120px;">
                                WEBSHOP
                        </button>
                    </li>
                    <li class="nav-item ms-5">
                        <a class="nav-link text-light" href="/days">
                            <b>TAGEN<br>
                            <span class="text-info">&</span>
                                FEIERN
                            </b>
                        </a>
                    </li>
                    <li class="nav-item ms-5">
                        <a class="nav-link text-light" href="/events">
                            <b>BÜSUM<br>
                            <span class="text-info">&</span>
                                EVENTS
                            </b>
                        </a>
                        <button class="btn btn-outline-info bg-info text-light"style="border-radius: 0px; width: 135px;">
                            GUTSCHEINSHOP
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>    
</div>

