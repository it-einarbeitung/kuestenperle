<div>
    <div class="bg-danger py-5">
        <div class="d-none d-lg-block">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-lg-1 text-center">
                        <img src="/images/logo-jever.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-1 text-center">
                        <img src="/images/logo-schneider.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-1 text-center mt-4">
                        <img src="/images/logo-lemonaid.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-1 text-center mt-4">
                        <img src="/images/logo-charitea.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-1 text-center mt-4">
                        <img src="/images/logo-samova.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-1 text-center mt-4">
                        <img src="/images/logo-klindworth.svg" class="img-fluid">
                    </div>
                    
                    <div class="col-lg-1 text-center mt-4">
                        <img src="/images/logo-seeberger.svg">
                    </div>
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-3">
                    </div>
                    <div class="col-2 text-center">
                        <img src="/images/logo-jever.svg" class="img-fluid">
                    </div>
                    <div class="col-2"></div>
                    <div class="col-2 text-center">
                        <img src="/images/logo-schneider.svg" class="img-fluid">
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-2">
                    </div>
                    <div class="col-3 text-center">
                        <img src="/images/logo-lemonaid.svg" class="img-fluid">
                    </div>
                    <div class="col-2"></div>
                    <div class="col-3 text-center">
                        <img src="/images/logo-charitea.svg" class="img-fluid">
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-2">
                    </div>
                    <div class="col-3 text-center">
                        <img src="/images/logo-samova.svg" class="img-fluid">
                    </div>
                    <div class="col-2"></div>
                    <div class="col-3 text-center">
                        <img src="/images/logo-klindworth.svg" class="img-fluid">
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-5"></div>
                    <div class="col-3 text-center">
                        <img src="/images/logo-seeberger.svg" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="d-none d-lg-block">
                <div class="row mt-5">
                    <div class="col-lg-2 mt-5">
                        <p class="text-light h5 mt-5">
                            Besuchen Sie uns auf
                        </p>
                        <p class="text-light">
                            <i class="fab fa-instagram text-light h3"></i>
                            <i class="fab fa-facebook-f text-light h3 ms-3"></i>
                        </p>
                        <p class="text-light h5">
                            Newsletter abonieren
                        </p>
                        <p class="text-light h5">
                            English Version
                        </p>
                    </div>
                    <div class="col-lg-2 mt-5">
                        <p class="text-light h5 mt-5">
                            Kontakt & Anreise
                        </p>
                        <p class="text-light h5 mt-3">
                            Onlineshop
                        </p>
                        <p class="text-light h5 mt-3">
                            Jobs
                        </p>
                        <p class="text-light h5 mt-3">
                            Gutscheine
                        </p>
                    </div>
                    <div class="col-lg-3 text-center mt-5">
                        <img src="/images/logo-kuestenperle-negative.svg" class="img-fluid">
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2 mt-5">
                        <p class="text-light h5 mt-5">
                           Hotelbewertungen
                        </p>
                        <p class="text-light h5 mt-3">
                            Impressum
                        </p>
                        <p class="text-light h5 mt-3">
                            AGB
                        </p>
                        <p class="text-light h5 mt-3">
                            Datenschutz
                        </p>
                    </div>
                    <div class="col-lg-2 mt-5">
                        <img src="/images/logo-eu-new.svg" class="img-fluid mt-5">
                    </div>
                </div>
            </div>
            <div class="d-block d-lg-none">
                <div class="row mt-5">
                    <div class="col-1">
                    </div>
                    <div class="col-10 text-center mt-5">
                        <img src="/images/logo-kuestenperle-negative.svg" class="img-fluid">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-3 text-start">
                        <p class="text-light h5">
                            Kontakt & Anreise
                        </p>
                        <p class="text-light h5 mt-3">
                            Onlineshop
                        </p>
                        <p class="text-light h5 mt-3">
                            Jobs
                        </p>
                        <p class="text-light h5 mt-3">
                            Gutscheine
                        </p>
                    </div>
                    <div class="col-2"></div>
                    <div class="col-3 text-start">
                        <p class="text-light h5">
                           Hotelbewertungen
                        </p>
                        <p class="text-light h5 mt-3">
                            Impressum
                        </p>
                        <p class="text-light h5 mt-3">
                            AGB
                        </p>
                        <p class="text-light h5 mt-3">
                            Datenschutz
                        </p>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-1">
                    </div>
                    <div class="col-11 text-center mt-5">
                        <img src="/images/logo-eu-new.svg" class="img-fluid">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-xl-3 ">
                        <p class="text-light h5">
                            Besuchen Sie uns auf
                        </p>
                        <p class="text-light">
                            <i class="fab fa-instagram text-light h3"></i>
                            <i class="fab fa-facebook-f text-light h3 ms-3"></i>
                        </p>
                        <p class="text-light h5">
                            Newsletter abonieren
                        </p>
                        <p class="text-light h5">
                            English Version
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
