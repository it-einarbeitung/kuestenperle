<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/events/buesum-events--freeform--slot-4_0.jpg" class="img-fluid">
        </div>
        <div class="row mb-5">
            <div class="col-lg-12 text-center mt-5">
                <p class="text-info text-center h1 mt-5">
                    <b>Lorem Ipsum</b>
                </p>
                <p class="text-dark text-center h5 mt-5">
                    Pellentesque blandit arcu sapien, tempor fermentum nunc varius ut
                </p>
                <p class="text-dark text-center h5">
                     Praesent aliquam justo vitae urna placerat scelerisque. Praesent id felis sit amet purus rutrum 
                </p>
                <p class="text-dark text-center h5">
                      Nullam diam enim, venenatis sed faucibus imperdiet, fringilla a orci. Cras id ligula eget  
                </p>
                <p class="text-dark text-center h5">
                      varius. Curabitur at orci lorem. Ut et porta metus. Pellentesque non erat elit. Sed molestie  
                </p>
            </div>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="d-none d-lg-block">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 text-center" style="margin-top:-100px;">
                    <img src="/images/events/eform--slot-1_0.jpg" class="img-fluid">
                </div>
                <div class="col-lg-5 text-center mt-5">
                    <i class="fas fa-anchor text-info h1 mt-5"></i>
                    <p class="text-info h1 mt-3">
                        <b>Where can I get some?</b>
                    </p>
                    <p class="text-light h5 mt-3">
                        There are many variations of passages of Lorem Ipsum available, 
                    </p>
                    <p class="text-light h5 mt-3">
                        but the majority have suffered alteration in some form, by injected  
                    </p>
                    <p class="text-light h5 mt-3">
                        humour, or randomised words which don't look even slightly  
                    </p>
                    <p class="text-light h5 mt-3">
                        believable. If you are going to use a passage of Lorem Ipsum, you   
                    </p>
                    <p class="text-light h5 mt-3">
                        hneed to be sure there isn't anything embarrassing hidden in the  
                    </p>
                    <p class="text-light h5 mt-3">
                        middle of text. All the Lorem Ipsum generators on the Internet   
                    </p>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3">
                    <img src="/images/events/eeform--slot-2_0.jpg" class="img-fluid">
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center mt-5">
                    <img src="/images/events/buesum-events--freeform--slot-4_0.jpg" class="img-fluid mt-5">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 text-center" style="margin-bottom:-222px;">
                    <img src="/images/events/rm--slot-5.jpg" class="img-fluid">
                </div>
            </div>
        </div>

        <div class="d-block d-lg-none">
            <div class="row">
                <div class="col-md-4 text-center ms-5" style="margin-top:-100px; width: 350px;">
                    <img src="/images/events/eform--slot-1_0.jpg" class="img-fluid">
                </div>
              
                <div class="col-lg-5 text-center mt-5">
                    <i class="fas fa-anchor text-info h1 mt-5"></i>
                    <p class="text-info h1 mt-3">
                        <b>Where can I get some?</b>
                    </p>
                    <p class="text-light h5 mt-3">
                        There are many variations of passages of Lorem Ipsum available, 
                    </p>
                    <p class="text-light h5 mt-3">
                        but the majority have suffered alteration in some form, by injected  
                    </p>
                    <p class="text-light h5 mt-3">
                        humour, or randomised words which don't look even slightly  
                    </p>
                    <p class="text-light h5 mt-3">
                        believable. If you are going to use a passage of Lorem Ipsum, you   
                    </p>
                    <p class="text-light h5 mt-3">
                        hneed to be sure there isn't anything embarrassing hidden in the  
                    </p>
                    <p class="text-light h5 mt-3">
                        middle of text. All the Lorem Ipsum generators on the Internet   
                    </p>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3">
                    <img src="/images/events/eeform--slot-2_0.jpg" class="img-fluid">
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center mt-5">
                    <img src="/images/events/buesum-events--freeform--slot-4_0.jpg" class="img-fluid mt-5">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 text-center" style="margin-bottom:-222px;">
                    <img src="/images/events/rm--slot-5.jpg" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5 py-5">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-info h1 mt-5">
                <b>What is Lorem Ipsum?</b>
            </p>
            <p class="text-dark text-center h5 mt-3">
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium 
            </p>
            <p class="text-dark text-center h5">
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </p>
            <p class="text-dark text-center h5">
                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
            </p>
            <p class="text-dark text-center h5">
                qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/images/events/deshow--slide-1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/events/buesum-events--slideshow--slide-2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/events/buesum-events--slideshow--slide-3.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Previous
                    </span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Next
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
