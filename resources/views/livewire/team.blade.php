<div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img src="/images/team/websiteplanet-dummy-1920X550.jpeg" class="img-fluid">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-info h1">
                <b>Eine Perle,die</b>
            </p> 
            <p class="text-info h1">
                <b>Küstenperle.</b>
            </p>
            <p class="text-dark h5 mt-5">
                Hier fühlen sich alle wohl. Für ein Wochenende zu zweit oder im Urlaub mit den 
            </p>
            <p class="text-dark h5">
                Lieben. Insbesondere Groß & Klein stehen bei uns im Fokus, von Familie zu 
            </p>
            <p class="text-dark h5">
                Familie. Denn wir sind ein sehr persönlich geführtes 4*-Haus - mit dem Restaurant 
            </p>
            <p class="text-dark h5">
                Deichperle, der Bar Schneiders sowie dem Spa- & Wellnessbereich zum Seele- 
            </p>
            <p class="text-dark h5">
                baumeln-lassen. Und das alles mit Watt & Strand direkt vor der Haustür. 
            </p>
            <p class="text-info mt-3">
                <a href="#" class="btn btn-link">
                    <b>Download Hotelbroschüre</b>
                </a>
            </p>
        </div>
    </div>
    <div class="bg-danger py-5 mt-5">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 text-end">
                    <img src="/images/hotel--freeform-1--slot-1.jpg" class="img-fluid" style="margin-top: -135px;">
                </div>
                <div class="col-lg-6 text-center mt-5">
                    <i class="fas fa-dove text-info h1 mt-5"></i>
                    <p class="text-info h1">
                       <b> Nordisch. Frisch.<br> Fröhlich.</b>
                    </p>
                    <p class="text-light h5 mt-4">
                        … heißt es in der Küstenperle, dem neuen Vier-Sterne-Strandhotel & Spa in  
                    </p>
                    <p class="text-light h5">
                        Büsum. Hier fühlt sich jeder wohl – allein, zu zweit und insbesondere auch 
                    </p>
                    <p class="text-light h5">
                        Familien. Das Restaurant Deichperle, die Bar Schneiders und der Spa- & 
                    </p>
                    <p class="text-light h5">
                        Wellnessbereich zeichnen uns aus. Und natürlich die liebevoll eingerichteten 
                    </p>
                    <p class="text-light h5">
                        Zimmer und Suiten. Alles mit frisch-nordisch-gemütlichem Flair.
                    </p>
                    <button type="button" class="btn btn-outline-info bg-info text-light h4 mt-3">
                        <b>3D RUNDGANG STARTEN</b>
                    </button>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-4">
                    <img src="/images/team/hotel--freeform-1--slot-3.jpg" class="img-fluid" >
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 text-center mt-5">
                    <img src="/images/team/hotel--freeform-1--slot-4.jpg" class="img-fluid mt-5">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 text-center mt-5" style="margin-bottom:-270px;">
                    <div id="carouselExampleIndicators" class="carousel slide"data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5">
                            </button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/frontpage--slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-5.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Previous
                            </span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Next
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-3"></div>
                <div class="col-lg-3 text-center">
                    <img src="/images/hotel--freeform-1--slot-1.jpg" class="img-fluid" style="margin-top: -135px; width:300px;">
                </div>
                <div class="col-lg-6 text-center mt-5">
                    <i class="fas fa-dove text-info h1 mt-5"></i>
                    <p class="text-info h1">
                       <b> Nordisch. Frisch.<br> Fröhlich.</b>
                    </p>
                    <p class="text-light h5 mt-4">
                        … heißt es in der Küstenperle, dem neuen Vier-Sterne-Strandhotel & Spa in  
                    </p>
                    <p class="text-light h5">
                        Büsum. Hier fühlt sich jeder wohl – allein, zu zweit und insbesondere auch 
                    </p>
                    <p class="text-light h5">
                        Familien. Das Restaurant Deichperle, die Bar Schneiders und der Spa- & 
                    </p>
                    <p class="text-light h5">
                        Wellnessbereich zeichnen uns aus. Und natürlich die liebevoll eingerichteten 
                    </p>
                    <p class="text-light h5">
                        Zimmer und Suiten. Alles mit frisch-nordisch-gemütlichem Flair.
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-2 text-center">
                    <img src="/images/team/hotel--freeform-1--slot-3.jpg" class="img-fluid" style="width:300px;" >
                </div>
                <div class="col-lg-4 text-center mt-5">
                    <img src="/images/team/hotel--freeform-1--slot-4.jpg" class="img-fluid mt-5" style="width: 300px;">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 text-center mt-5">
                    <div id="carouselExampleIndicators" class="carousel slide"data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4">
                            </button>
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5">
                            </button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/images/frontpage--slideshow-1--slide-1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-2.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-3.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-4.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/images/frontpage--slideshow-1--slide-5.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Previous
                            </span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true">
                            </span>
                            <span class="visually-hidden">
                                Next
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-5">
       <div class="row mt-5">
           <div class="col-lg-12 text-center mt-5">
               <p class="text-info h1 mt-5">
                   <b>Moin, moin. Das Team der </b>
               </p> 
               <p class="text-info h1">
                   <b>Küstenperle.</b>
               </p>
               <p class="text-dark h5 mt-5">
                   Dürfen wir uns vorstellen? Wir, das sind als Direktoren-Quartett: Ehepaar Isa und David Schneider 
               </p>
               <p class="text-dark h5">
                   ebenso wie Andrea und Jürgen Kahlke, Isas Eltern. Die Idee zur Küstenperle war zunächst ein 
               </p>
               <p class="text-dark h5">
                   Herzenswunsch und ist dann im Kreise unserer Familie gewachsen und groß geworden.
               </p>
           </div>
       </div>
       <div class="row mt-5">
           <div class="col-lg-5 text-center ">
               <img src="/images/team/hotel--freeform-2--slot-1.jpg" class="img-fluid">
           </div>
           <div class="col-lg-1"></div>
           <div class="col-lg-6 text-center mt-5">
               <img src="/images/team/hotel--freeform-2--slot-2.jpg" class="img-fluid mt-5">
               <p class="text-dark h5 mt-5">
                   Allen Gästen eine wunderbare „Fofftein“ bereiten können wir jedoch nur 
               </p>
               <p class="text-dark h5 ">
                   gemeinsam mit einem starken Team. Einige unserer Mitarbeiter sind extra 
               </p>
               <p class="text-dark h5 ">
                   hierher an die Küste gezogen, andere kennen die Region wie ihre  
               </p>
               <p class="text-dark h5 ">
                   Westentasche. So bunt unsere Mannschaft auch ist - alle sind mit einem   
               </p>
               <p class="text-dark h5 ">
                   Lächeln dabei, wenn es heißt: „Willkommen in der Küstenperle“.   
               </p>
           </div>
       </div>
       <div class="row mt-5">
           <div class="col-lg-3 text-center mt-5">
               <img src="/images/team/Sina Röpke.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-5">
               <img src="/images/team/Jessica Skott-Baumgarten.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-5">
               <img src="/images/team/Michell Meister.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-5">
               <img src="/images/team/hotel-team-member-2-bastianinselmann.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
       </div>
       <div class="row mt-3">
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team--member-image-4-new.png" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team-member-4-matthiaskrueger.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team--member-image-5-new.png" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/Facetune.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
       </div>
       <div class="row mt-3">
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team-member-7-christinreisewitz.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team-member-9-katharina-lutze.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/hotel-team--member-image-5-new.png" class="img-fluid" style="border-radius:150px;">
           </div>
           <div class="col-lg-3 text-center mt-3">
               <img src="/images/team/Facetune.jpg" class="img-fluid" style="border-radius:150px;">
           </div>
       </div>
    </div>
</div>
