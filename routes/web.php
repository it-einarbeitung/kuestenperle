<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::get('/welcome',\App\Http\Livewire\Welcome::class)->name('welcome');

Route::get('/team',\App\Http\Livewire\Team::class)->name('team');

Route::get('/room',\App\Http\Livewire\Room::class)->name('room');

Route::get('/restaurant',\App\Http\Livewire\Restaurant::class)->name('restaurant');

Route::get('/wellness',\App\Http\Livewire\Wellness::class)->name('wellness');

Route::get('/days',\App\Http\Livewire\Days::class)->name('days');

Route::get('/events',\App\Http\Livewire\Events::class)->name('events');